public class Controller {
    private Service service;

    public Controller () {
        service = new Service();
    }

    public String init() {
        return "Init Controller";
    }

    public String introduce() {
        return "Perkenalkan saya Controller";
    }

}