public class Service {
    private Repository repository;
    private static String nama = "Developer";   

    public Service () {
        repository = new Repository();
    }

    public String init() {
        return "Init Service";
    }

    public String sayBye() {
        return "Saya "+nama+" undur diri, see yaa!!";
    }
    
    public String sayHi() {
        return "Hi Buddy, I am " + nama;
    }

}